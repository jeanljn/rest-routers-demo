const { Router } = require('express')
const { compile } = require('path-to-regexp')

const rewrite = (pairs = {}) => {
  const router = Router()

  const rewriter = (path) => (req, _, next) => {
    req.url = compile(path, { encode: encodeURIComponent })(req.params)

    next()
  }

  Object.entries(pairs).forEach(([from, to]) => {
    router.all(from, rewriter(to))
  })

  return router
}

module.exports = rewrite