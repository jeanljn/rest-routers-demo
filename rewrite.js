const express = require('express')

const employeesRouter = require('./routers/employees')
const projectsRouter = require('./routers/projects')

const rewrite = require('./utils/rewrite')

const app = express()

app.use(express.json())

// employees API was in german previously, we need to allow both german and english now
app.use(rewrite({
  '/mitarbeiter': '/employee',
  '/mitarbeiter/:employeeId': '/employee/:employeeId',
  '/mitarbeiter/:employeeId/pramie': '/employee/:employeeId/bonus',
  '/mitarbeiter/:employeeId/pramie/:bonusId': '/employee/:employeeId/bonus/:bonusId',
}))

app.use((req, res, next) => {
  if (req.url === '/hey') req.url = '/employee'

  next()
})

app.use('/employee', employeesRouter)
app.use('/project', projectsRouter)

app.use((err, req, res, next) => res.status(404).json(err.message))

app.listen(21560, () => console.log('Server listening'))
