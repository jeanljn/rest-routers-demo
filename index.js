const express = require('express')

const employeesRouter = require('./routers/employees')
const projectsRouter = require('./routers/projects')

const app = express()

app.use(express.json())


app.use('/employee', employeesRouter)
app.use('/project', projectsRouter)

app.use((err, req, res, next) => res.status(404).json(err.message))

app.listen(21560, () => console.log('Server listening'))
