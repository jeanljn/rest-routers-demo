const express = require('express')

process.env.DMZ = 1

const employeesRouter = require('./routers/employees')
const projectsRouter = require('./routers/projects.dmz')
const auth = require('./auth.middleware')

const app = express()

app.use(express.json())

// public
app.use('/project', projectsRouter)
// auth
app.use(auth)
// private
app.use('/employee', employeesRouter)

app.use((err, req, res, next) => res.status(err.code ?? 404).json(err.message))

app.listen(21560, () => console.log('Server listening'))
