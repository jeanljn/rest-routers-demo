# REST Routers demo

Une petite démo pour montrer comment organiser facilement des routeurs et sous-routeurs (sans limite de profondeur d'imbrication, à priori) en fonction des préfixes REST choisis.

## Installation

```bash
npm i
```

:warning: Les jeux de requêtes utilisent le plugin VSCode [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

## Structuration

- L'application n'utilise pas de routeur principal, j'ai branché directement les autres routeurs dessus, mais c'est tout à fait envisageable et même conseillé pour une grosse application.
- Les routeurs `employees` et `projects` sont montés sur l'application, le routeur `bonuses` est monté sur le routeur `employees`
  - Parce que le routeur `bonuses` est monté sur un préfixe contenant le param `employeeId`, les routes de ce routeur ont accès à `req.employee` (notez que le `router.param` n'est pas hérité, si le routeur enfant est monté sur un préfixe sans param et que ses routes mentionnent ce param, le middleware correspondant ne sera pas exécuté)
- Les routes des sous-routeurs sont très courtes, ce qui évite pas mal de répétitions. En guise de mémo, on peut utiliser JSDoc en haut du routeur pour rappeler sur quel préfixe sera monté le routeur.

```bash
npm run start
```

Pour tester :
- [Le CRUD des employés](./employees.http)
- [Le CRUD des projets](./projects.http)
- [Le CRUD des primes des employés](./bonuses.http)

## DMZ

```bash
npm run start:dmz
```

Pour gérer l'auth en limitant les répétitions, on peut organiser les routeurs comme dans [cet exemple](./dmz.js), testable avec [ce jeu de requêtes](./dmz.http). Le scénario mis en place ici expose le CRUD des projets tout en protégeant celui des employés et des bonus.

Notez que la matérialisation d'une DMZ n'empêche pas de protéger unitairement des routes au sein de celles-ci : dans l'exemple, les projets sont publiques mais la suppression est restreinte.

Il reste également possible de procéder à des vérifications supplémentaires sur certaines routes, par exemple autoriser uniquement le propriétaire d'une prime à la modifier/supprimer.

## Réécriture

```bash
npm run start:rewrites
```

Un moyen très simple pour gérer de la réécriture d'URL (et ainsi maintenir la retrocompatibilité d'une API Rest) est d'utiliser le module `path-to-regexp` d'Express pour réaiguiller des requêtes qui matchent d'anciennes routes vers les nouvelles routes. Une démo est [disponible ici](./rewrite.js), testable avec [ce jeu de requêtes](./employees.german.http). Le scénario en place ici est une ancienne version du CRUD des employés, en allemand, qu'on souhaite maintenir en parallèle de la nouvelle version en anglais.

Notez que les noms des params doivent être les mêmes dans la clé et la valeur de chaque paire d'URLs.