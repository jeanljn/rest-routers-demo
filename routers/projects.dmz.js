const projects = require('../data/projects.json')
const { Router } = require('express')
const auth = require('../auth.middleware')

const router = Router()

/**
 * @baseurl /project
 */

router.param('projectId', (req, res, next, pid) => {
  const project = projects.find(({ id }) => id === Number(pid))

  if (!project) return next(new Error('Unknown project id'))

  req.project = project
  next()
})

router.route('/')
  .get((req, res) => res.json(projects))
  .post((req, res) => {
    const newItem = { id: projects[projects.length - 1].id + 1, ...req.body }
    projects.push(newItem)
    res.status(201).json(newItem)
  })

router.route('/:projectId')
  .get((req, res) => res.json(req.project))
  .put((req, res) => {
    const editedItem = { id: Number(req.params.projectId), ...req.body }
    projects.splice(projects.indexOf(req.project), 1, editedItem)
    res.json(editedItem)
  })
  .delete(
    auth, // on peut toujours utiliser notre MW localement
    (req, res) => {
      projects.splice(projects.indexOf(req.project), 1)
      res.sendStatus(204)
    }
  )

module.exports = router
