const bonuses = require('../data/bonuses.json')
const { Router } = require('express')

const router = Router()

/**
 * @baseurl /employee/{id}/bonus
 * @context req.employee - The employee entity whose id is passed in the url
 */

router.param('bonusId', (req, res, next, eid) => {
  const bonus = req.bonuses.find(({ id }) => id === Number(eid))

  if (!bonus) return next(new Error('Unknown bonus id'))

  req.bonus = bonus
  next()
})

router.use((req, res, next) => {
  req.bonuses = bonuses.filter(({ employeeId }) => employeeId === req.employee.id)

  next()
})

router.route('/')
  .get((req, res) => res.json(req.bonuses))
  .post((req, res) => {
    const newItem = {
      id: bonuses[bonuses.length - 1].id ?? 0 + 1,
      employeeId: req.employee.id,
      ...req.body,
    }
    bonuses.push(newItem)
    res.status(201).json(newItem)
  })

router.route('/:bonusId')
  .get((req, res) => res.json(req.bonus))
  .put((req, res) => {
    const editedItem = {
      id: Number(req.params.bonusId),
      employeeId: req.employee.id,
      ...req.body,
    }
    bonuses.splice(bonuses.indexOf(req.bonus), 1, editedItem)
    res.json(editedItem)
  })
  .delete((req, res) => {
    bonuses.splice(bonuses.indexOf(req.bonus), 1)
    res.sendStatus(204)
  })

module.exports = router
