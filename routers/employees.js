const employees = require('../data/employees.json')
const bonusesRouter = require('./bonuses')
const { Router } = require('express')

const router = Router()

/**
 * @baseurl /employee
 */

router.param('employeeId', (req, res, next, eid) => {
  const employee = employees.find(({ id }) => id === Number(eid))

  if (!employee) return next(new Error('Unknown employee id'))

  req.employee = employee
  next()
})

router.route('/')
  .get((req, res) => res.json(employees))
  .post((req, res) => {
    const newItem = { id: employees[employees.length - 1].id + 1, ...req.body }
    employees.push(newItem)
    res.status(201).json(newItem)
  })

router.route('/:employeeId')
  .get((req, res) => res.json(req.employee))
  .put((req, res) => {
    const editedItem = { id: Number(req.params.employeeId), ...req.body }
    employees.splice(employees.indexOf(req.employee), 1, editedItem)
    res.json(editedItem)
  })
  .delete((req, res) => {
    employees.splice(employees.indexOf(req.employee), 1)
    res.sendStatus(204)
  })

router.use('/:employeeId/bonus', bonusesRouter)

module.exports = router
