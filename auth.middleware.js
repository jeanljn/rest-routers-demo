module.exports = (req, res, next) => {
  if (!req.get('authorization') || req.get('authorization') !== 'w473r9473') return next({ message: 'Unauthorized access', code: 403 })
  next()
}
